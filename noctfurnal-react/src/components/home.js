import '../App.css';

function Home() {
    return (
        <section id="home">
        <div className="home-container">
          <h1>Noctfurnal <i className="fas fa-paw fa-xs"></i></h1>
          <h2>We are a friendly furry Discord community.</h2>
          <a href="#about" className="btn-get-started">Awoo!</a>
        </div>
      </section>
    )
}

export default Home;