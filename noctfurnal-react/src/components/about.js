import '../App.css'
import {useState} from 'react'
import axios from 'axios';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

function About() {
    function getCount() {
       axios.get('https://discord.com/api/v8/')
    }
    let guildMemberCount = useState("")
    return (
        <section id="about">
    <div className="container">
      <div className="row about-container">

        <div className="col-lg-6 content order-lg-1 order-2">
          <h2 className="title">About Us</h2>
          <p>
            Noctfurnal was originally made with the goal of bringing furries together to find new friends and chill. It has now expanded into a <a id="redirect-links"href="https://discord.noctfurnal.com">Discord server</a> with  members and a partnered <a id="redirect-links" href="https://telegram.noctfurnal.com">Telegram group</a> with over 150 members.
          </p>

          <div className="icon-box wow fadeInUp">
            <div className='icon'><FontAwesomeIcon icon={['fas', 'user-friends']} color="blue" size='2x'/></div>
            <h4 className="title"><a href="">Friendly staff</a></h4>
            <p className="description">Our friendly staff team is always available to help whenever you have a question.</p>
          </div>

          <div className="icon-box wow fadeInUp" data-wow-delay="0.2s">
            <div className="icon"><FontAwesomeIcon icon={['fas', 'gift']} color="blue"/></div>
            <h4 className="title"><a href="">Giveaways</a></h4>
            <p className="description">At certain member milestones, giveaways are held. So far, we've given away Discord Nitro and paysafecard codes.</p>
          </div>

          <div className="icon-box wow fadeInUp" data-wow-delay="0.4s">
            <div className="icon"><i className="fas fa-hashtag"></i></div>
            <h4 className="title"><a href="">Many channels</a></h4>
            <p className="description">There are lots of awoomazing channels for you to chat in! From memes to art; share anything you can think of.</p>
          </div>

        </div>
      </div>

    </div>
  </section>
    )
}

export default About

