import '../App.css'
import logo from '../img/icon.png'
import {Nav, Navbar, Container} from 'react-bootstrap'

function Header() {
    return (
      <Container>
      <Navbar collapseOnSelect fixed="top"  expand="sm" variant="dark" className="#header-nav">
        
        <Navbar.Brand href="#home"><img src={logo} alt="noctfur-logo" width="30" height="30" classname="logo" /></Navbar.Brand>
        <Navbar.Toggle aria-controls='responsive-navbar-nav' />
        <Navbar.Collapse id='responsive-navbar-nav'>
          <Nav>
            <Nav.Link href='#home'>Home</Nav.Link>
            <Nav.Link href="#about">About</Nav.Link>
            <Nav.Link href="#stats">Stats</Nav.Link>
            <Nav.Link href="#team">Staff Team</Nav.Link>
            <Nav.Link href="https://discord.noctfurnal.com">Join Us</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        </Navbar>
      </Container>
    )
}

export default Header;