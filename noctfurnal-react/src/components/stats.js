function Stats() {
    return (
        <div>
            <section id="stats">
    <div className="container wow fadeIn">
      <div className="section-header">
        <h3 className="section-title">Stats</h3>
        <p className="section-description">A few statistics about our server...</p>
      </div>
      <div className="row counters">

        <div className="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">0</span>
          <p>Total Members</p>
        </div>

        <div className="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">0</span>
          <p>Members Online</p>
        </div>

        <div className="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">0</span>
          <p>Channels</p>
        </div>

        <div className="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">0</span>
          <p>Roles</p>
        </div>

      </div>

    </div>
  </section>
        </div>
    )
}

export default Stats