import About from "./about";
import Header from "./header";
import Home from "./home";
import React from "react";
import Stats from './stats'


import './lib/bootstrap/css/bootstrap.min.css';
class MainWebpage extends React.Component {

    render() {
        return (
            <div className='noctfurnal-main'>
             <Header />
             <Home />
             <About />
             <Stats />
            </div>
            ) 
    }
    
    
    
}

export default MainWebpage