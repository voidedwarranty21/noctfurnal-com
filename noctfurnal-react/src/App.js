
import './App.css';
import {BrowserRouter, Route} from 'react-router-dom'
import MainWebpage from './components/mainWebpage';
import Callback from './components/callback';
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'

library.add(fas)

function App() {
  function request() {
  
  }
  return (
    <BrowserRouter>
    <Route exact path='/' component={MainWebpage} />
    <Route exact path='/callback' component={Callback} />
    </BrowserRouter>
    

  )
}

export default App;
